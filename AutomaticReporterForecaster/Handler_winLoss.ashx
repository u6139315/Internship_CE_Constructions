﻿<%@ WebHandler Language="C#" Class="Handler_winLoss" %>

using System;
using System.Web;

public class Handler_winLoss : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string result = context.Request["term"];
        string re = "[\"" + result + "/" + (100 - int.Parse(result)).ToString() + "\"]";
        context.Response.Write(re);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}