﻿CREATE TABLE [dbo].[companies] (
    [name]       VARCHAR (50) NOT NULL,
    [contact]    VARCHAR (50) NULL,
    [mSpend]     MONEY        NULL,
    [phone]      VARCHAR (50) NULL,
    [website]    VARCHAR (50) NULL,
    [suburb]     VARCHAR (50) NULL,
    [street]     VARCHAR (50) NULL,
    [introducer] VARCHAR (50) NULL,
    [regDate]    DATE         NULL,
    PRIMARY KEY CLUSTERED ([name] ASC)
);

CREATE TABLE [dbo].[CompanySystems] (
    [company]    VARCHAR (50) NOT NULL,
    [coreSystem] VARCHAR (50) NOT NULL
);

CREATE TABLE [dbo].[Contacts] (
    [Company]  VARCHAR (50) NOT NULL,
    [Name]     VARCHAR (50) NOT NULL,
    [Phone]    VARCHAR (50) NULL,
    [Email]    VARCHAR (50) NULL,
    [suburb]   VARCHAR (50) NULL,
    [street]   VARCHAR (50) NULL,
    [mobile]   VARCHAR (50) NULL,
    [jobTitle] VARCHAR (50) NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([Company] ASC, [Name] ASC)
);

CREATE TABLE [dbo].[dailyReport] (
    [Name]             VARCHAR (50)   NOT NULL,
    [Date]             DATE           NOT NULL,
    [CallType]         VARCHAR (50)   NOT NULL,
    [Customer]         VARCHAR (50)   NOT NULL,
    [Contact]          VARCHAR (50)   NOT NULL,
    [Notes]            VARCHAR (1023) NOT NULL,
    [FollowUpDeligate] VARCHAR (50)   NULL,
    [project]          VARCHAR (50)   NULL,
    [quote]            VARCHAR (50)   NULL
);

CREATE TABLE [dbo].[Highlights] (
    [name]    VARCHAR (50)   NOT NULL,
    [date]    DATE           NOT NULL,
    [content] VARCHAR (1023) NOT NULL,
    CONSTRAINT [PK_Highlights] PRIMARY KEY CLUSTERED ([name] ASC, [date] ASC)
);

CREATE TABLE [dbo].[IndustryInfo] (
    [name]    VARCHAR (50)   NOT NULL,
    [date]    DATE           NOT NULL,
    [content] VARCHAR (1023) NOT NULL,
    CONSTRAINT [PK_IndustryInfo] PRIMARY KEY CLUSTERED ([name] ASC, [date] ASC)
);

CREATE TABLE [dbo].[people] (
    [username] VARCHAR (50) NOT NULL,
    [password] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([username] ASC)
);

CREATE TABLE [dbo].[Project] (
    [name]     VARCHAR (50) NOT NULL,
    [suburb]   VARCHAR (50) NULL,
    [street]   VARCHAR (50) NULL,
    [company]  VARCHAR (50) NULL,
    [contact]  VARCHAR (50) NULL,
    [estValue] MONEY        NULL,
    PRIMARY KEY CLUSTERED ([name] ASC)
);

CREATE TABLE [dbo].[ProjectSystems] (
    [project]    VARCHAR (50) NOT NULL,
    [coreSystem] VARCHAR (50) NOT NULL
);

CREATE TABLE [dbo].[quotes] (
    [number]   NVARCHAR (10) NOT NULL,
    [name]     VARCHAR (20)  NULL,
    [date]     DATE          NULL,
    [customer] VARCHAR (50)  NULL,
    [value]    MONEY         NULL,
    [product]  VARCHAR (50)  NULL,
    [winLoss]  NVARCHAR (5)  NULL,
    CONSTRAINT [PK_quotes] PRIMARY KEY CLUSTERED ([number] ASC)
);

