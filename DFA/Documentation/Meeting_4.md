# Meeting_4

Venue: 30 Geelong Street, Fyshwick ACT 2609 

Date: 11th August 2017

Participants: Hao, Daniel

---

In this meeting I showed my discoveries of the suppliers given to me. Also, I needed to prepare for a presentation #3 to the management team. The dialogs following are fabricated because I can't recall exactly the same ones.

---

H: Hello Daniel. I have put products from the 4 suppliers you assigned to me into a Excel form. But I have met some troubles.

D: What are they?

H: Well, some products can't be found on the suppliers' website. And some datasheets are webpage themselves instead of PDF's. What should I do?

D: In that case, you will have to **distinguish different error types** using your program.

H: Okay, I'll have a look into it.

( a couple of minutes later)

H: Hey, I have made the modification. Now, the error log have an column showing the error types in addition to columns existed in the input Excel form.

D: That looks good. I think you have nearly done the project. I need you to show your work to the management team next Monday. What do you say?

H: Sure, I can do the presentation #3.

D: Are you comfortable with speaking in front of people?

H: Yes... I am a little nervous when it comes to public speaking.

D: No worries. I'll invite Michael and Damion to the meeting. Just three audiences.

H: Well, I guess I will be less uncomfortable then.

D: Cool. During the presentation, you will need to
```agenda
 how excel holds/ references data
 how the program works
 how data looks like in the system
 show "error" reports
 show basic steps for data entry
```

H: Roger.