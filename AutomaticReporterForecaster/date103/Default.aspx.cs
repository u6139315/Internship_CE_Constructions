﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    private SqlConnection conn;
    private SqlCommand cmd;
    private List<string> li = new List<string>();
    //<ui diplay functions>
    void hide_customer_details()
    {
        HiddenField_customer.Value = "";
        customer_details_js.Text = "<script type='text/javascript'>$(function(){$(\"#customer_details\").hide();});</script>";
    }
    void show_customer_details()
    {
        customer_details_js.Text = "<script type='text/javascript'>$(function(){$(\"#customer_details\").show();});</script>";
        HiddenField_customer.Value = "new";
    }
    void hide_project()
    {
        project_js.Text = "<script>$(function(){$(\"#project\").hide();});</script>";
        Label_keyItem_js.Text = "";
        TextBox_dr_keyItem.Style.Add("visibility", "hidden");
    }
    void show_project()
    {
        project_js.Text = "<script>$(function(){$(\"#project\").show();});</script>";
        Label_keyItem_js.Text = "Key items to follow up";
        TextBox_dr_keyItem.Style.Add("visibility", "shown");
    }
    void hide_project_details()
    {
        project_details_js.Text = "<script>$(function(){$(\"#project_details\").hide();});</script>";
        HiddenField_project.Value = "";
    }
    void show_project_details()
    {
        project_details_js.Text = "<script>$(function(){$(\"#project_details\").show();});</script>";
        HiddenField_project.Value = "new";
    }
    void hide_contact_details()
    {
        HiddenField_contact.Value = "";
        contact_details_js.Text = "<script>$(function(){$(\"#contact_details\").hide();});</script>";
    }
    void show_contact_details()
    {
        contact_details_js.Text = "<script>$(function(){$(\"#contact_details\").show();});</script>";
        HiddenField_contact.Value = "new";
    }
    void hide_quote_details()
    {
        HiddenField_quote.Value = "";
        quote_details_js.Text = "<script>$(function(){$(\"#quote_details\").hide();});</script>";
    }
    void show_quote_details()
    {
        HiddenField_quote.Value = "new";
        quote_details_js.Text = "<script>$(function(){$(\"#quote_details\").show();});</script>";
        TextBox_dr_q_customer.Text = TextBox_dr_customer.Text;
    }
    //</ui diplay functions>
    protected void Page_Load(object sender, EventArgs e)
    {
        //GridView_calls.Attributes.Add("style", "word-break:break-all;word-wrap:normal");
        if (Session["name"] == null) Server.Transfer("signIn.aspx");
        Label1.Text = "Welcome " + Session["name"].ToString()+"!";        
        if (!IsPostBack)//initiaslization
        {
            TextBox_dr_date.Text = DateTime.Now.ToShortDateString();
            TextBox_proj_date.Text = DateTime.Now.ToShortDateString();
            TextBox_proj_rep.Text = Session["name"].ToString();
            DateTime dt = DateTime.Now;
            while (dt.DayOfWeek != DayOfWeek.Friday)
            {
                dt = dt.AddDays(1);
            }
            string we = dt.ToShortDateString();
            string ws = dt.AddDays(-7).ToShortDateString();
            Session["weekEnd"] = we;
            tb_sfsc_end.Text = we;
            TextBox_sq_end.Text = we;
            tb_sfsc_start.Text = ws;
            TextBox_sq_start.Text = ws;
            List<string> wes = new List<string>();
            DateTime mD;
            try
            {
                mD = DateTime.Parse(Class1.scaler("SELECT MIN(date) FROM dailyReport;"));
            }
            catch
            {
                mD = DateTime.Now;
            }
            TimeSpan ts = new TimeSpan(7, 0, 0, 0);
            do
            {
                wes.Add(dt.ToShortDateString());
                dt = dt.Subtract(ts);
            } while (dt >= mD);
            DropDownList_wr_weekending.DataSource = wes;
            hide_project();
            //to do when first time loaded
        }
        if (Request["view"] == null) MultiView1.SetActiveView(View1);
        else
        {
            switch (Request["view"].ToString())
            {
                case "0":
                    MultiView1.SetActiveView(View0);
                    break;
                case "1":
                    MultiView1.SetActiveView(View1);
                    //Button2_Click(null, null);
                    break;
                case "2":
                    MultiView1.SetActiveView(View2);
                    DropDownList_wr_weekending.DataBind();
                    break;
                case "3":
                    MultiView1.SetActiveView(View3);
                    break;
                case "sfsc":
                    MultiView1.SetActiveView(View_sfsc);
                    break;
                case "5":
                    MultiView1.SetActiveView(View5);
                    break;
                case "sfq":
                    MultiView1.SetActiveView(view_sq);
                    break;
                case "quotes":
                    MultiView1.SetActiveView(view_quotes);
                    break;
                case "sys":
                    MultiView1.SetActiveView(view_sys);
                    GridView_projSys.DataBind();
                    GridView_cpnSys.DataBind();
                    break;
                case "h":
                    MultiView1.SetActiveView(View_highlights);
                    break;
                case "i":
                    MultiView1.SetActiveView(View_industry);
                    break;
                case "k":
                    MultiView1.SetActiveView(View_keyItem);
                    break;
            }
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        GridView_calls.DataBind();
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Server.Transfer("signIn.aspx");
        Session.Clear();
        Session.Abandon();
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        //add records in other tables if necessary
        if (HiddenField_customer.Value != "")
        {
            //new customer
            Class1.execute("INSERT INTO companies VALUES('" +
                TextBox_dr_customer.Text + "','" +
                TextBox_dr_contact.Text + "','" +
                TextBox_dr_monthlySpend.Text + "','" +
                TextBox_dr_phone.Text + "','" +
                TextBox_dr_website.Text + "','" +
                TextBox_dr_suburb.Text + "','" +
                TextBox_dr_street.Text + "','" +
                Session["name"] + "',CONVERT(DATE,'" +
                TextBox_dr_date.Text + "',103));");
            hide_customer_details();
            if (TextBox_dr_customer.Text != "" && TextBox_dr_coreSys.Text != "")
            {
                //core system
                foreach(string s in TextBox_dr_coreSys.Text.Split('/'))
                {
                    Class1.execute("INSERT INTO companySystems VALUES('" +
                        TextBox_dr_customer.Text + "','" +
                        s + "');");
                }
            }
        }
        if (HiddenField_project.Value != "")
        {
            //new project
            Class1.execute("INSERT INTO project VALUES('" +
                TextBox_dr_projName.Text + "','" +
                TextBox_dr_pSuburb.Text + "','" +
                TextBox_dr_pStreet.Text + "','" +
                TextBox_dr_pCompany.Text + "','" +
                TextBox_dr_pContact.Text + "','" +
                TextBox_dr_pValue.Text + "','" +
                Session["name"] + "',CONVERT(DATE,'" + 
                TextBox_dr_date.Text + "',103));");
            hide_project_details();
            if (TextBox_dr_pCore.Text != "" && TextBox_dr_projName.Text!="")
            {
                //project core system
                foreach (string s in TextBox_dr_pCore.Text.Split('/'))
                {
                    Class1.execute("INSERT INTO projectSystems VALUES('"+
                        TextBox_dr_projName.Text + "','"+
                        s + "');");
                }
            }
        }
        if (HiddenField_contact.Value != "")
        {
            //new contact
            Class1.execute("INSERT INTO contacts VALUES('" +
                TextBox_dr_customer.Text + "','" +
                TextBox_dr_contact2.Text + "','" +
                TextBox_dr_phone2.Text + "','" +
                TextBox_dr_email.Text + "','" +
                TextBox_dr_suburb2.Text + "','" +
                TextBox_dr_street2.Text + "','" +
                TextBox_dr_mobile.Text + "','" +
                TextBox_dr_jobTitle.Text + "');");
            hide_contact_details();
        }
        if (HiddenField_quote.Value != "")
        {
            Class1.execute("INSERT INTO quotes VALUES('" +
                TextBox_dr_number.Text + "','" +
                Session["name"] + "',CONVERT(DATE,'" +
                TextBox_dr_date.Text + "',103),'" +
                TextBox_dr_q_customer.Text + "','" +
                TextBox_dr_value.Text + "','" +
                TextBox_dr_product.Text + "','" +
                TextBox_dr_winLoss.Text + "');");
            hide_quote_details();
        }
        //using parameters to enable single apostrophies
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("@name", Session["name"].ToString());
        //dic.Add("@date", TextBox_dr_date.Text);
        dic.Add("@type", DropDownList4.SelectedValue);
        dic.Add("@customer", TextBox_dr_customer.Text);
        dic.Add("@contact", TextBox_dr_contact2.Text);
        dic.Add("@notes", TextBox_dr_notes.Text);
        dic.Add("@delegate", TextBox_dr_delegate.Text);
        dic.Add("@proj", TextBox_dr_projName.Text);
        dic.Add("@quote", TextBox_dr_number.Text);
        Class1.execute("INSERT INTO dailyReport VALUES(" +
            "@name,CONVERT(DATE,'"+TextBox_dr_date.Text+"',103),@type,@customer,@contact,@notes,@delegate,@proj,@quote);",dic);
        GridView_calls.DataBind();//this line refreshes the grid view
        if (TextBox_dr_highlight.Text != "")
        {
            Dictionary<string, string> dic2 = new Dictionary<string, string>();
            dic2.Add("@name", Session["name"].ToString());
            dic2.Add("@date", TextBox_dr_date.Text);
            dic2.Add("@content", TextBox_dr_highlight.Text);
            try
            {
                Class1.execute("INSERT INTO highlights VALUES(@name,CONVERT(DATE,@date,103),@content);",dic2);
            }catch(Exception ecp)
            {
                if (ecp is SqlException)
                {
                    SqlException secp = (SqlException)ecp;
                    switch (secp.Number)
                    {
                        case 2627:  //duplications
                            Class1.execute("UPDATE highlights SET content={fn CONCAT(content, @content)} WHERE name=@name AND date=CONVERT(DATE,@date,103);", dic2);
                            break;
                    }
                }
                else throw;
            }
            
        }
        if (TextBox_dr_industry.Text != "")
        {
            Dictionary<string, string> dic3 = new Dictionary<string, string>();
            dic3.Add("@name", Session["name"].ToString());
            dic3.Add("@date", TextBox_dr_date.Text);
            dic3.Add("@content", TextBox_dr_industry.Text);
            try
            {
                Class1.execute("INSERT INTO industryinfo VALUES(@name,CONVERT(DATE,@date,103),@content);", dic3);
            }
            catch (Exception ecp)
            {
                if (ecp is SqlException)
                {
                    SqlException secp = (SqlException)ecp;
                    switch (secp.Number)
                    {
                        case 2627:  //duplications
                            Class1.execute("UPDATE industryinfo SET content={fn CONCAT(content, @content)} WHERE name=@name AND date=CONVERT(DATE,@date,103);", dic3);
                            break;
                    }
                }
                else throw;
            }
        }
        if (TextBox_dr_keyItem.Text != "" && TextBox_dr_projName.Text!="")
        {
            Dictionary<string, string> dic4 = new Dictionary<string, string>();
            dic4.Add("@project", TextBox_dr_projName.Text);
            dic4.Add("@content", TextBox_dr_keyItem.Text);
            try
            {
                Class1.execute("INSERT INTO keyItem VALUES(@project,@content);", dic4);
            }
            catch (Exception ecp)
            {
                if (ecp is SqlException)
                {
                    SqlException secp = (SqlException)ecp;
                    switch (secp.Number)
                    {
                        case 2627:  //duplications
                            Class1.execute("UPDATE keyItem SET content={fn CONCAT(content, @content)} WHERE project=@project;", dic4);
                            break;
                    }
                }
                else throw;
            }
        }
    }

    protected void Button_0_insert_Click(object sender, EventArgs e)
    {
        Class1.execute("INSERT INTO contacts VALUES('" +
            TextBox_contacts_company.Text + "','" +
            TextBox_contacts_name.Text + "','" +
            TextBox_contacts_phone.Text + "','" +
            TextBox_contacts_email.Text + "','" +
            TextBox_contacts_suburb.Text+"','"+
            TextBox_contacts_street.Text+"','"+
            TextBox_contacts_mobile.Text+"','"+
            TextBox_contacts_jobTitle.Text+"');");
        GridView_contacts.DataBind();
    }

    protected void Button_0_show_Click(object sender, EventArgs e)
    {
        Class1.load("SELECT * FROM contacts;", GridView1);
    }

    protected void Button_company_insert_Click(object sender, EventArgs e)
    {
        Class1.execute("INSERT INTO companies VALUES('" +
            TextBox_company_name.Text + "','" +
            TextBox_company_contact.Text + "','"+
            TextBox_company_monthlySpend.Text+"','"+
            TextBox_company_phone.Text+"','"+
            TextBox_company_website.Text+"','"+
            TextBox_company_suburb.Text+"','"+
            TextBox_company_street.Text+"','"+
            Session["name"].ToString()+"',CONVERT(DATE,'"+
            DateTime.Now.ToShortDateString()+"',103));");
        GridView_companies.DataBind();
    }

    protected void Button_company_show_Click(object sender, EventArgs e)
    {
        Class1.load("SELECT * FROM companies;", GridView1);
    }

    protected void View0_Activate(object sender, EventArgs e)
    {
        GridView_contacts.DataBind();
    }

    protected void View1_Activate(object sender, EventArgs e)
    {
        return;
    }

    protected void Button_q_add_Click(object sender, EventArgs e)
    {
        Class1.execute("INSERT INTO quotes VALUES('" +
            TextBox_q_number.Text + "','" +
            TextBox_q_name.Text + "',CONVERT(DATE,'" +
            TextBox_q_date.Text + "',103),'" +
            TextBox_q_customer.Text + "','" +
            TextBox_q_value.Text + "','" +
            TextBox_q_product.Text + "','" +
            TextBox_q_winloss.Text + "')");
        GridView_quotes.DataBind();
    }

    protected void View2_Activate(object sender, EventArgs e)
    {
        string we = DropDownList_wr_weekending.SelectedValue;
        if (we == "" || we == null) we = Session["weekEnd"].ToString();
        //update tables:quotes, projects, business, highlights, industry, keyIdemForFollowUp
        
        //zero leading format date string can be compared as date correctly
        we = "CONVERT(DATE,'"+we+"',103)";
        Class1.load("SELECT * FROM quotes WHERE date <= "+
            we + " AND DATEADD(day,7,date) > " +
            we +";", GridView_wr_quotes);
        string project_full = "SELECT " +
            "name,suburb,street,company,contact,estvalue,regdate";
        string project_query_tail = 
            " FROM project WHERE rep='" +
            Session["name"].ToString() + "' AND " +
            "regdate <=" +
            we +
            " AND " +
            we +
            "< DATEADD(DAY,7,regdate)";
        Class1.load(project_full+project_query_tail, GridView_wr_projects);
        Class1.load("SELECT * FROM companies WHERE regdate <= " +
            we + " AND DATEADD(day,7,regdate) > " +
            we + "AND introducer = '"+
            Session["name"].ToString()+"';", GridView_wr_business);
        Class1.load("SELECT content FROM highlights WHERE " +
            "name='" + Session["name"].ToString() + "' AND" +
            " date<="+ we +" AND "+
            we+" < DATEADD(DAY,7,[date]);", GridView_wr_highlights);
        Class1.load("SELECT content FROM industryinfo WHERE name='" +
            Session["name"].ToString() + "' AND " +
            we + " BETWEEN [date] AND DATEADD(DAY,6,[date]);", GridView_wr_industry);
        //update keyitem
        Class1.load("SELECT * FROM keyItem where project IN (select name "+project_query_tail+");", GridView_wr_keyItemForFollowUp);
    }


    protected void Button_sfsc_search_Click(object sender, EventArgs e)
    {
        string cus = TextBox_sfsc_customer.Text, proj = TextBox_sfsc_project.Text;
        cus += "%";
        proj += "%";
        DateTime dt = new DateTime();
        if (DateTime.TryParse(tb_sfsc_start.Text,out dt)
            && DateTime.TryParse(tb_sfsc_end.Text, out dt))
        {
            Class1.load("SELECT [Name],CONVERT(VARCHAR(10),[Date],111) as date,[CallType],[Customer],[Contact],[Notes],[FollowUpDeligate],[project],[quote] FROM dailyReport WHERE date >=" +
                " CONVERT(DATE,'"+tb_sfsc_start.Text+"',103) AND "+
                "CONVERT(DATE,'"+tb_sfsc_end.Text+"',103) >= date AND customer LIKE '"+
                cus+"' AND project LIKE '"+proj+"'; ", GridView_sfsc);
        }
        else
        {
            Class1.load("SELECT [Name],CONVERT(VARCHAR(10),[Date],111) as date,[CallType],[Customer],[Contact],[Notes],[FollowUpDeligate],[project],[quote] FROM dailyReport WHERE customer LIKE '" +
                cus + "' AND project LIKE '" + proj + "'; ", GridView_sfsc);
        }
        //GridView_sfsc.Columns[6].HeaderStyle.Width = 50;
        GridView_sfsc.HeaderStyle.Width = 20;
    }

    protected void TextBox_dr_date_TextChanged(object sender, EventArgs e)
    {
        DateTime dt=new DateTime();
        if (DateTime.TryParse(TextBox_dr_date.Text, out dt))
        {
            Session["date"] = TextBox_dr_date.Text;
        }
        else
        {
            TextBox_dr_date.Text = Session["date"].ToString();
        }
    }

    protected void Button_sq_Click(object sender, EventArgs e)
    {
        DateTime dt = new DateTime();
        string start = TextBox_sq_start.Text;
        string end = TextBox_sq_end.Text;
        string customer = TextBox_sq_customer.Text;
        if (DateTime.TryParse(end,out dt) && DateTime.TryParse(start,out dt))
        {
            start = DateConvert.convert(start);
            end = DateConvert.convert(end);
            Class1.load("SELECT [number],[name],CONVERT(VARCHAR(10),[date],103),[customer],[value],[product],[winLoss] FROM quotes WHERE " +
            (customer != "" ? "customer = '" + customer + "' AND " : "") +
            "date BETWEEN CONVERT(DATE,'" + start + "',103) AND CONVERT(DATE,'" + end + "',103) AND " +
            "value >= CAST('" + TextBox_sq_value_lower.Text + "' AS money)", GridView_sq);
        }
        else
        {
            Class1.load("SELECT [number],[name],CONVERT(VARCHAR(10),[date],111),[customer],[value],[product],[winLoss] FROM quotes WHERE " +
            (customer != "" ? "customer = '" + customer + "' AND " : "") +
            "value >= CAST('" + TextBox_sq_value_lower.Text + "' AS money)", GridView_sq);
        }
    }

    protected void GridView_calls_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        if (e.CommandName == "d")
        {
            int index=(int.Parse(e.CommandArgument.ToString()));
            string name = Session["name"].ToString();
            string date = Session["date"].ToString();
            string type = GridView_calls.Rows[index].Cells[0].Text.ToString();
            string customer = GridView_calls.Rows[index].Cells[1].Text.ToString();
            string contact = GridView_calls.Rows[index].Cells[2].Text.ToString();
            string notes = GridView_calls.Rows[index].Cells[3].Text.ToString();
            string follow = GridView_calls.Rows[index].Cells[4].Text.ToString();
            string project = GridView_calls.Rows[index].Cells[5].Text.ToString();
            string quote = GridView_calls.Rows[index].Cells[6].Text.ToString();
            Class1.delete_calls(index, name, "CONVERT(DATE,'"+date+"',103)", type, customer, contact, notes, follow, project, quote);
            GridView_calls.DataBind();
        }
    }

    protected void GridView_sfsc_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        if (e.CommandName == "d")
        {
            int index = (int.Parse(e.CommandArgument.ToString()));
            string name = GridView_sfsc.Rows[index].Cells[1].Text.ToString();
            string date = GridView_sfsc.Rows[index].Cells[2].Text.ToString();
            string type = GridView_sfsc.Rows[index].Cells[3].Text.ToString();
            string customer = GridView_sfsc.Rows[index].Cells[4].Text.ToString();
            string contact = GridView_sfsc.Rows[index].Cells[5].Text.ToString();
            string notes = GridView_sfsc.Rows[index].Cells[6].Text.ToString();
            string follow = GridView_sfsc.Rows[index].Cells[7].Text.ToString();
            string project = GridView_sfsc.Rows[index].Cells[8].Text.ToString();
            string quote = GridView_sfsc.Rows[index].Cells[9].Text.ToString();
            Class1.delete_calls(index, name, "'"+date+"'", type, customer, contact, notes, follow, project, quote);
            Button_sfsc_search_Click(null, null);
        }
    }

    protected void GridView_sq_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        if (e.CommandName == "d")
        {
            int index = (int.Parse(e.CommandArgument.ToString()));
            string number = GridView_sq.Rows[index].Cells[1].Text.ToString();
            Class1.execute("DELETE FROM quotes WHERE number='" + number + "';");
            GridView_sq.DataBind();
        }
    }

    protected void TextBox_dr_customer_TextChanged(object sender, EventArgs e)
    {
        string s = Class1.scaler("SELECT COUNT(*) FROM companies WHERE name='" + TextBox_dr_customer.Text + "'");
        if (s == "1")
        {
            hide_customer_details();
        }
        else
        {
            show_customer_details();
        }
    }

    protected void TextBox_dr_projName_TextChanged(object sender, EventArgs e)
    {
        string s = Class1.scaler("SELECT COUNT(*) FROM project WHERE name = '" + TextBox_dr_projName.Text + "';");
        if (s == "0")
        {
            //show project details
            show_project_details();
        }
        else
        {
            hide_project_details();
        }
    }

    protected void TextBox_dr_contact2_TextChanged(object sender, EventArgs e)
    {
        string s = Class1.scaler("SELECT COUNT(*) FROM contacts WHERE company='" + TextBox_dr_customer.Text + "' AND name='" + TextBox_dr_contact2.Text + "';");
        if (s == "0")
        {
            show_contact_details();
        }
        else
        {
            hide_contact_details();
        }
    }

    protected void TextBox_dr_number_TextChanged(object sender, EventArgs e)
    {
        string s = Class1.scaler("SELECT COUNT(*) FROM quotes WHERE number='" +TextBox_dr_number.Text+ "';");
        if (s == "0")
        {
            show_quote_details();
        }
        else
        {
            hide_quote_details();
        }
    }

    protected void Button_proj_save_Click(object sender, EventArgs e)
    {
        Class1.execute("INSERT INTO project(name,contact,estValue,suburb,street,company,rep,regDate) VALUES('" +
            TextBox_proj_name.Text + "','" +
            TextBox_proj_contact.Text + "','" +
            TextBox_proj_monthlySpend.Text + "','" +
            TextBox_proj_suburb.Text + "','" +
            TextBox_proj_street.Text + "','" +
            TextBox_proj_customer.Text + "','" +
            TextBox_proj_rep.Text + "',CONVERT(DATE,'" +
            TextBox_proj_date.Text + "',103));");
        GridView_project.DataBind();
    }

    protected void Button_highlight_add_Click(object sender, EventArgs e)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("@name1", TextBox_highlights_name.Text);
        //dic.Add("@date", TextBox_highlights_date.Text);
        dic.Add("@content", TextBox_highlights_content.Text);
        Class1.execute("INSERT INTO highlights VALUES(@name1,CONVERT(DATE,'"+TextBox_highlights_date.Text+"',103),@content);",dic);
        GridView_highlights.DataBind();
    }

    protected void Button_i_Click(object sender, EventArgs e)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("@name1", TextBox_i_name.Text);
        //dic.Add("@date", TextBox_i_date.Text);
        dic.Add("@content", TextBox_i_content.Text);
        Class1.execute("INSERT INTO industryinfo VALUES(@name1,CONVERT(DATE,'"+ TextBox_i_date.Text + "',103),@content);", dic);
        GridView_i.DataBind();
    }

    protected void Button_sys_proj_Click(object sender, EventArgs e)
    {
        Class1.execute("INSERT INTO projectSystems VALUES('" +
            TextBox_sys_proj.Text + "','" +
            TextBox_sys_system.Text + "');");
        GridView_projSys.DataBind();
    }

    protected void GridView_projSys_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            int i = int.Parse(e.CommandArgument.ToString());
            Class1.execute("DELETE FROM projectSystems WHERE project='" +
                GridView_projSys.Rows[i].Cells[0].Text.ToString() + "' AND coreSystem='" +
                GridView_projSys.Rows[i].Cells[1].Text.ToString() + "';");
            GridView_projSys.DataBind();
        }
    }

    protected void Button_sys_customer_Click(object sender, EventArgs e)
    {
        Class1.execute("INSERT INTO companySystems VALUES('" +
            TextBox_sys_customer.Text + "','" +
            TextBox_sys_system2.Text + "');");
        GridView_cpnSys.DataBind();
    }

    protected void GridView_cpnSys_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            int i = int.Parse(e.CommandArgument.ToString());
            Class1.execute("DELETE FROM companySystems WHERE company='" +
                GridView_cpnSys.Rows[i].Cells[0].Text.ToString() + "' AND coreSystem='" +
                GridView_cpnSys.Rows[i].Cells[1].Text.ToString() + "';");
            GridView_cpnSys.DataBind();
        }
    }

    protected void TextBox_dr_contact_TextChanged(object sender, EventArgs e)
    {
        TextBox_dr_contact2.Text = TextBox_dr_contact.Text;
    }

    protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(DropDownList4.SelectedValue.ToString()=="Project")
        {
            show_project();
        }
        else
        {
            hide_project();
        }
    }

    protected void Button_keyItem_Click(object sender, EventArgs e)
    {
        Dictionary<string, string> dic4 = new Dictionary<string, string>();
        dic4.Add("@project", TextBox_keyItem_project.Text);
        dic4.Add("@content", TextBox_keyItem_content.Text);
        Class1.execute("INSERT INTO keyItem VALUES(@project,@content);", dic4);
        GridView_keyItem.DataBind();
    }
}