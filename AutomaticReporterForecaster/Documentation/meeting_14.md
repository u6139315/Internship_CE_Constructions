Date:13/10/2017

Purpose: discuss fields with a sales person, the purpose, relationship, etc.

Expectation: doubts get clarified

Record Keeper: Hao

Attendees: David, Hao

Agenda:
- the field of "core systems" contains multiple systems, which are separated by foreward slashes.
- the field of "key items for follow up" is tricky because it represent the same information as a sales call in a more concise way. We should leave it alone for a while until we meet with Daniel for further instructions.
- a bug has been found. When David tris to input a single apostrophy, the sys raises an error. That is because SQL query use this to represent a string, once extra ones are in, the DBMS will be confused.
