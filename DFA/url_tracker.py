import tkinter
import tkinter.filedialog
import tkinter.ttk
from tkinter.constants import *

from urllib.request import urlretrieve
from urllib.request import urlopen
from os import getcwd
from os.path import exists
from os import mkdir
from time import sleep
import sys

import threading

def set_input_file(fileName,*buttons):
    # assume fileName is a list with one element
    fileName[0]=tkinter.filedialog.askopenfilename(initialdir=getcwd(),filetypes=[('Comma Separated Vectors', '*.csv')])
    if f[0]:
        for b in buttons:
            b.config(state=NORMAL)
        print("selected file:",fileName[0])
    else:
        print("please choose a file")

def set_output_file(lines):
    fobj=tkinter.filedialog.asksaveasfile(initialdir=getcwd(),filetypes=[('Comma Separated Vectors', '*.csv')])
    if fobj:
        fobj.writelines(lines)
        fobj.close()
        print("error log saved")

def creatShortcut(name, link, good):
    f = open(name+".url", 'w')
    f.write('[InternetShortcut]\n')
    f.write('URL="'+link+'"\n')
    f.write('IconFile=%SystemRoot%\\system32\\SHELL32.dll\n')
    f.write('IconIndex='+str(55 if good else 77)+'\n')
    f.close()

def check(progressbar,source,errorLog,nextBtn):
    with open(source) as fobj:
        l_work = fobj.readlines()
        increment = 100/len(l_work)
        print("--->\t","checking\t","<---")
        cwd = getcwd()
        for line in l_work:
            components = line.strip().split(',')
            directory = cwd+'\\'+components[0]
            if not exists(directory):
                mkdir(components[0])
            print('retrieving from ', components[0], components[1])
            try:
                h = urlopen(components[2])
                creatShortcut(components[0]+'/'+components[1], components[2], True)
                print('__successfully loaded')
            except ValueError:
                print('skipped because of absence of url')
                continue;
            except Exception as exc:
                print('[error]',exc)
                print(type(exc))
                errorLog.append(line.strip()+','+str(exc)+'\n')
                creatShortcut(components[0]+'/'+components[1], components[2], False)
            progressbar.step(increment)
            progressbar.update()
        print("--->\t","checked\t","<---")
    if errorLog:
        # enable button "save as"
        nextBtn.config(state=NORMAL)

def download(progressbar,source,errorLog,nextBtn):
    with open(source) as fobj:
        l_work = fobj.readlines()
        increment = 100/len(l_work)
        cwd = getcwd()
        print("--->\t","downloading\t","<---")
        for line in l_work:
            components = line.strip().split(',')
            directory = cwd+'\\'+components[0]
            if not exists(directory):
                mkdir(components[0])
            print('retrieving from ', components[0], components[1])
            try:
                urlretrieve(components[2],directory + '\\'+components[1])
                print('__successfully loaded')
            except ValueError:
                print('skipped because of absence of url')
                continue;
            except Exception as exc:
                print('[error]',exc)
                print(type(exc))
                errorLog.append(line.strip()+','+str(exc)+'\n')
            finally:
                progressbar.step(increment)
                progressbar.update()
        print("--->\t","downloaded\t","<---")
    if errorLog:
        # enable button "save as"
        nextBtn.config(state=NORMAL)

if __name__=="__main__":
    tk = tkinter.Tk()
    f = [None]
    errors=[]

    frame = tkinter.Frame(tk, relief=RIDGE, borderwidth=2)
    frame2 = tkinter.Frame(frame)
    label = tkinter.Label(frame, text="Open a file to get started")
    label2 = tkinter.Label(frame, text="the file should be in a CSV format")
    label3 = tkinter.Label(frame, text="and the three columns are:")
    label4 = tkinter.Label(frame, text="subdirectory, filename, and the URL")
    label5 = tkinter.Label(frame, text="copyright © Hao Wang, 2017")
    label6 = tkinter.Label(frame2,text=" or ")
    pb = tkinter.ttk.Progressbar(frame,orient=HORIZONTAL,length=200,mode='determinate')
    btn_save=tkinter.Button(frame,text="export error log",command=lambda:set_output_file(errors),state=DISABLED)
    btn_check=tkinter.Button(frame2,text="check",command=lambda:check(pb,f[0],errors,btn_save),state=DISABLED)
    btn_download=tkinter.Button(frame2,text="download",command=lambda:download(pb,f[0],errors,btn_save),state=DISABLED)
    btn_open=tkinter.Button(frame,text="open",command=lambda :set_input_file(f,btn_check,btn_download))

    frame.pack(fill=BOTH,expand=1)
    label.pack(fill=X, expand=1)
    label2.pack()
    label3.pack()
    label4.pack()
    btn_open.pack(fill=X,padx=10)
    frame2.pack()
    btn_check.pack(fill=X,side=LEFT)
    label6.pack(side=LEFT)
    btn_download.pack(fill=X,side=RIGHT)
    pb.pack()
    btn_save.pack(fill=X,padx=10)
    label5.pack()

    tk.mainloop()
