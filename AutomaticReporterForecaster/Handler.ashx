﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Generic;

public class Handler : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/json";
        string[] ss = Class1.load("SELECT customer FROM dailyReport WHERE customer LIKE '"+context.Request["term"]+"%' GROUP BY customer ORDER BY COUNT(*) DESC;");
        JavaScriptSerializer jss = new JavaScriptSerializer();
        string s = jss.Serialize(ss);
        context.Response.Write(s);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}