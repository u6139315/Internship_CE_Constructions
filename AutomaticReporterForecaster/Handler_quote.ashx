﻿<%@ WebHandler Language="C#" Class="Handler_quote" %>

using System;
using System.Web;
    using System.Web.Script.Serialization;

public class Handler_quote : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/json";
        string[] ss = Class1.load("SELECT number FROM quotes WHERE number LIKE '"+context.Request["term"]+"%'");
        JavaScriptSerializer jss = new JavaScriptSerializer();
        string s = jss.Serialize(ss);
        context.Response.Write(s);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}