# Meeting_2

Venue: 30 Geelong Street, Fyshwick ACT 2609 

Date: 4th August 2017

Participants: Hao, Daniel

---

In this meeting I discussed a possible way to refine the prototype with Daniel.  The dialogs following are fabricated because I can't recall exactly the same ones.

---

H: Hi, Daniel.

D: What's the matter?

H: I am wondering what should we do if we can't access some datasheets with the old URL's.

D: Oh, right. Some datasheets may have been updated by their producers. Okay, in that case, we need to know which product has this issue. Can your program do this for me?

H: Of course. It's just **an exception handling procedure**.

D: I'm counting on you.

( after several hours...)

H: Hello, Daniel. I have come up with an idea of error log to record those errors. What do you say?

D: That's great. If we got the log, then our people would only have to manually check those reported products. There are probably many products are done automatically. Can you have a check with some suppliers to see the percentage of feasible products?

H: Not a problem. What supplier do you need me to check?

D: I'll give you... A, B, C and D. ( for confidential reasons, the actual names are hidden)

H: That would mean I need to manually enter those products of the suppliers you give to me. Can you pick some easy ones for me? I think it's better to just have a quick check.

D: I know you are not supposed to do the data entry job. But that's necessary for us to find out how it works. I guarantee you that after you finished this project, I have another fun project for you.

H: No worries. I understand. And I really wish to apply some machine learning techniques to the next project. We may talk about that later.

D: Sure. Now, begin your investigation.

H: Copy that.