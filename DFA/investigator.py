import sys
from urllib.request import urlretrieve
from os.path import exists

if __name__=='__main__':
    '''
    this script takes one argument as the list of links
    then it will download corresponsive files
    in the end, an error log will be generated
    '''
    l = sys.argv[1:]
    f_log = open('error_log.csv','a')
    with open(l[0]) as work_list:
        for line in work_list:
            components = line.strip().split('/')
            print('retrieving from ', line)
            try:
                urlretrieve(line,components[-1])
                print('__successfully loaded')
            except Exception as exc:
                print('[error]',exc)
                f_log.write(line.strip()+','+str(exc)+'\n')
    f_log.close()
