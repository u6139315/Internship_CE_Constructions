﻿<%@ WebHandler Language="C#" Class="Handler_project" %>

using System;
using System.Web;
    using System.Web.Script.Serialization;

public class Handler_project : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/json";
        string[] ss = Class1.load("SELECT name FROM project WHERE name LIKE '"+context.Request["term"]+"%'");
        JavaScriptSerializer jss = new JavaScriptSerializer();
        string s = jss.Serialize(ss);
        context.Response.Write(s);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}