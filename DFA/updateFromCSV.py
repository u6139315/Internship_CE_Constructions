from urllib.request import urlretrieve
from urllib.request import urlopen
from os import getcwd
from os.path import exists
from os import mkdir
from time import sleep
import sys

def creatShortcut(name, link, good):
    f = open(name+".url", 'w')
    f.write('[InternetShortcut]\n')
    f.write('URL="'+link+'"\n')
    f.write('IconFile=%SystemRoot%\\system32\\SHELL32.dll\n')
    f.write('IconIndex='+str(55 if good else 77)+'\n')
    f.close()

if __name__ == '__main__':
    theList = sys.argv[1]
    errorLog = sys.argv[2]
    f_log = open(errorLog,'a')
    with open(theList) as work_list:
        cwd = getcwd()
        for line in work_list:
            components = line.strip().split(',')
            directory = cwd+'\\'+components[0]
            if not exists(directory):
                mkdir(components[0])
            print('retrieving from ', components[0], components[1])
            try:
                h = urlopen(components[2])
                creatShortcut(components[0]+'/'+components[1], components[2], True)
                print('__successfully loaded')
            except ValueError:
                print('skipped because of absence of url')
                continue;
            except Exception as exc:
                print('[error]',exc)
                print(type(exc))
                f_log.write(line.strip()+','+str(exc)+'\n')
                creatShortcut(components[0]+'/'+components[1], components[2], False)
    f_log.close()
