﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="signUp.aspx.cs" Inherits="signUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>create an account</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Table ID="Table1" runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        username:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error"
                            ControlToValidate="TextBox1"
                            ID="RequiredFieldValidator1"
                            runat="server" ErrorMessage="please input a username"></asp:RequiredFieldValidator>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>password:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="error"
                            ControlToValidate="TextBox2"
                            ErrorMessage="please input a password"></asp:RequiredFieldValidator>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>confirm password:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="error"
                            ControlToValidate="TextBox3"
                            ControlToCompare="TextBox2"
                            Operator="Equal"
                            Type="String"
                            ErrorMessage="the two passwords are not the same!"></asp:CompareValidator>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click"/>
            <asp:Button ID="Button2" runat="server" Text="Return" OnClick="Button2_Click" CausesValidation="false"/>
        </div>
    </form>
</body>
</html>
