# Meeting_8

Venue: 30 Geelong Street, Fyshwick ACT 2609 

Date: 25th August 2017

Participants: Hao, Daniel

---

The dialogs following are fabricated because I can't recall exactly the same ones.

---

D: Hao, how did you do on the suppliers?

H: Good I guess. I have looked into about a hundred suppliers.

D: What's the percentage of accessible suppliers？

H: I don't have an exact number. However, there is a list I keep note on. As you can see, many suppliers are ok.

D: What kind of errors do you have for those inaccessible suppliers?

H: I have two kinds of errors. One is hTTP404 and the other is Http403. As for HTTP404, there is no round-about. However, HTTP403 error can be solved by referencing the links instead of downloading, which makes me think we should just use links rather than PDF's.

D: Can you make those links similar to PDF's?

H: I guess so. You know, in Windows OS, you can create shortcuts for almost anything, including webpages. All you need is to right click your muse inside a folder and then choose NEW -> CREATE SHORTCUT.

( here pops a window asking for the location of the resource referenced)

D: So we need to insert a link here?

H: Yeah, you got it.

D: Great. Can you find someway to **populate this kind of shortcut in the folders**?

H: I'm on it.

D: By the way, sometimes I may put **relevant information** like pricelist, contract, testing results in the folder. Will your program **leave them as they are** or wipe them out?

H: Of course they will be intact. I guarantee it.

D: Awesome, let me know when you finished.

H: Copy that.