Date:16/10/2017

Purpose: discuss possible ways to publish the web application I am working on

Expectation: how to hand over my web app to my client's hosting service provider

Record Keeper: Hao

Attendees: Simon, Hao

Agenda:
- Simon introduced me how their service works: server,DB -> firewall -> web -> customers
- Simon told me that their firewall is very basic and the security level isn't very high. That's why they are using email server to keep things secure.
- Simon convinced me that security is more significant than applications
- Then he told me for security reason, customers shouldn't be able to input information into the server or DB, which means my web application won't work on their server once they block the C2S channel.
- Simon suggested using WordPress and REST API with the web so that they can host it
