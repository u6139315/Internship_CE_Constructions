# Meeting_3

Venue: 30 Geelong Street, Fyshwick ACT 2609 

Date: 7th August 2017

Participants: Hao, Daniel

---

In this meeting I showed how the new feature ( generating the error log) works.  In addition, I was introduced another project ( a.k.a. Automatic reporter + input forecaster). The dialogs following are fabricated because I can't recall exactly the same ones.

---

H: Hi, Daniel. I have looked into the suppliers you gave  to me the other day.

D: How's it?

H: Let me show you.

D: That looks great! You've almost finished the job. Excellent! But there are still a couple week to go. Like I told you, I have another interesting project for you. Would you want to have a look?

H: Try me.

D: At present, we have reps who sells products to our customers. Those reps report weekly to me with a report. As you can see, here is an Excel form that those reps need to fill in.

![wkl rpt](weeklyReport.JPG)

H: So you store these Excel forms in your server?

D: Yes. I'd like to be able to reuse these forms or data. For now, these data are hard to retrieve as they are in many different files. Once I have read the report, it will be lost.

![info flow](communicationDiagram_old.jpg)

H: I see. It's like an digital black hole. Once you receive the information it will be lost soon.

D: Exactly.

H: I think you need a Database Management System to enable you to take advantage of the data you have.

D: Good! How long do you think it would take you to do the new project?

H: Perhaps 4 weeks, because I have to learn how to implement a DBMS from scratch.

D: Oky-dokey. Take your time.