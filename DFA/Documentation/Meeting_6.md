# Meeting_6

Venue: 30 Geelong Street, Fyshwick ACT 2609 

Date: 18th August 2017

Participants: Hao, Daniel

---

The dialogs following are fabricated because I can't recall exactly the same ones.

---

D: How's work?

H: I have looked into 40 suppliers.

D: Cool.

H: But, I have an issue to discuss with you.

D: Shoot.

H: After I received the new project. I'm thinking that maybe I can use some machine learning techniques to help you with the recommendation of the products you sell for your suppliers.

D: How does that work?

H: Well, basically, your business is to link between suppliers and your customers.

D: That's correct.

H: Then we can think like this. You have a function, which is defined in a mathematical way, and it takes the enquiry of your customers as input and return you a  product as output.

D: Hum, that's interesting.

H: Then we can use some model to link customers with products. For example, a neural network. Because a neural network is essentially an universal function approximate. As long as I get the data, I'm able to implement it.

D: I agree with you. But the problem is...  Let me put it this way.  The procedure of making a sales recommendation is very complex. When I am to make a recommendation, I normally will look into the price of the product, whether it's in our warehouse, technical datasheets, etc.

H: I see. If I want to implement that neural network, I will need a enormous data set. And to collect such a large data set, it will likely take me forever.

D: That's right. Sorry you can't use what you have learned in ANU. Our business is relatively small.

H: That's alright. I will try my best on the project whether or not it's an interesting one.

D: See you on Monday.

H: See you, have a good weekend.

D: You too, mate.