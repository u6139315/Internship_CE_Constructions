﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// DateConvert 的摘要说明
/// </summary>
public class DateConvert
{
    public static string convert(string shortDateString)
    {
        /// this will return sql formated date
        string[] l = shortDateString.Split('/');
        if (l[1].Length == 1) l[1] = "0" + l[1];
        if (l[2].Length == 1) l[2] = "0" + l[2];
        return l[0] + "/" + l[1] + "/" + l[2];
    }
    public DateConvert()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
    }
}