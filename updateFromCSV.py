from urllib.request import urlretrieve
from os import getcwd
from os.path import exists
from os import mkdir

if __name__ == '__main__':
    f_log = open('error_log'+'2'+'.csv','a')
    with open('list.csv') as work_list:
        cwd = getcwd()
        for line in work_list:
            components = line.strip().split(',')
            directory = cwd+'\\'+components[0]
            if not exists(directory):
                mkdir(components[0])
            print('retrieving from ', components[0], components[1])
            try:
                urlretrieve(components[2],directory + '\\'+components[1])
                print('__successfully loaded')
            except Exception as exc:
                print('[error]',exc)
                f_log.write(line.strip()+','+str(exc)+'\n')
    f_log.close()
