﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Web.UI.WebControls;


public class Class1
{
    public static string[] load(string query)
    {
        List<string> ss = new List<string>();
        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataReader reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            ss.Add(reader[0].ToString());
        }
        return ss.ToArray();
    }
    public static void load(string query,ListBox lb)
    {
        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        lb.DataSource = reader;
        
        lb.DataBind();
        conn.Close();
    }
    public static void load(string query,GridView gv)
    {
        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        gv.DataSource = reader;
        gv.DataBind();
        conn.Close();
    }
    public static void load(string query, DropDownList ddl)
    {
        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        List<string> li = new List<string>();
        while (reader.Read())
        {
            li.Add(reader[0].ToString());
        }
        ddl.DataSource = li;
        ddl.DataBind();
        conn.Close();
    }
    public static void execute(string query)
    {
        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        cmd.ExecuteNonQuery();
        conn.Close();
    }
    public static void execute(string query, Dictionary<string, string> dic)
    {
        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]);
        conn.Open();
        SqlCommand cmd = new SqlCommand(query, conn);
        foreach (var item in dic)
        {
            cmd.Parameters.Add(item.Key,SqlDbType.VarChar,1023);
            cmd.Parameters[item.Key].Value = item.Value;
        }
        cmd.ExecuteNonQuery();
        conn.Close();
    }
    public static bool userIsAuthorized(string user, string pwd)
    {
        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]);
        conn.Open();
        SqlCommand cmd = new SqlCommand("select count(*) from people where username=@uid and password=@pwd;",conn);
        cmd.Parameters.Add("@uid", SqlDbType.VarChar,50);
        cmd.Parameters.Add("@pwd", SqlDbType.VarChar, 50);
        cmd.Parameters["@uid"].Value = user;
        cmd.Parameters["@pwd"].Value = pwd;
        //using parameters to evade sql inject attacks
        SqlDataReader reader = cmd.ExecuteReader();
        if (reader.Read())
        {
            return (int)reader[0] > 0;
        }
        return false;
    }
    public static void delete_calls(int index, string name, string date, string type, string customer, 
        string contact, string notes, string follow, string project, string quote)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        customer = customer == "&nbsp;" ? "" : customer;
        contact = contact == "&nbsp;" ? "" : contact;
        notes = notes == "&nbsp;" ? "" : notes;
        follow = follow == "&nbsp;" ? "" : follow;
        project = project == "&nbsp;" ? "" : project;
        quote = quote == "&nbsp;" ? "" : quote;
        dic.Add("@name", name);
        //dic.Add("@date", date);
        dic.Add("@type", type);
        dic.Add("@customer", customer);
        dic.Add("@contact", contact);
        dic.Add("@notes", notes);
        dic.Add("@delegate", follow);
        dic.Add("@proj", project);
        dic.Add("@quote", quote);
        execute("DELETE FROM dailyReport WHERE " +
        "[name] = @name AND " +
        "[date] = '"+date+"' AND " +
        "[calltype] = @type AND " +
        "[customer] = @customer AND " +
        "[contact] = @contact AND " +
        "[quote] = @quote AND " +
        "[followupdeligate] = @delegate AND " +
        "[project] = @proj AND " +
        "[notes] = @notes;", dic);
    }
    public static string scaler(string query)
    {
        using (SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connStr"]))
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);
            return cmd.ExecuteScalar().ToString();
        }
    }
}