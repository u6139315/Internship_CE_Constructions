Date:16/10/2017

Purpose: discuss about the project

Expectation: keep Hao on the right track

Record Keeper: Hao

Attendees: Alaine, Hao

Agenda:
- Hao showed where his head is on the project
- Hao told Alaine the [meeting](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/blob/master/AutomaticReporterForecaster/Documentation/meeting_16.md) with Simon and its impact on the project
- Alaine gave several suggestions on how to manage changes in a general manner. (future enhancements)
- Alaine showed Hao how to do a planning and illustrated its importance
- Alaine suggested Hao to document the project technically and in a business origined manner.
- Alaine analysed what Hao needs to do in the coming 6 days of internship
