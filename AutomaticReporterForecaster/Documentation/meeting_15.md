Date:16/10/2017

Purpose: catch up with Daniel

Expectation: understand the business logic of quotes and my client's anticipation of search functions

Record Keeper: Hao

Attendees: Daniel, Hao

Agenda:
- Demonstrated functions to Daniel: entering sales calls information into the system, autocomplete funtion that can make data entering faster, UI design of hidden fields and the consitions on which they will show up
- Daniel told me the searching function i implemented for sales calls is excellent.
- Daniel told what he will use to search quotes: date and name. In addition, he wish to have a filter of quotes' value
- for quotes that are already won or lost, Daniel wishes to delete them so that they can focus on suspending quotes, which are business opportunities
