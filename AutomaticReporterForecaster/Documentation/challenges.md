# Challenges I've faced with and how I overcame them

### SQL injection threats

This is due to SQL's feature that a string can be both data and command. To solve this problem, I use SQL Parameter class to restrict a section of string to be only data.

### How to get the data from a SQL Server?

What's returned from a SQL Server once a query has been excecuted, can be read in a format. I searched the interfaces on the Internet. Using SqlDataReader is a good choice.

### how to use mapping in C#?

I learned that a generic Dictionary class would serve the purpose. To define it, Dictionary<string,string> d = new Dictionary<string,string>();.

To add key-value pairs, d.Add(key,value);. To traverse it, foreach(var in d){//do sth}

### In notes field, sometimes single apostrophies are used, which can lead to failure of a SQL query

I use SQL Parameter class to solve this problem.

### GridView cells don't warp automatically

Forcely add CSS format to it: GridView_calls.Attributes.Add("style", "word-break:break-all;word-wrap:normal");

### GridView doesn't refresh itself

In the function Page_Load(), call function DataBind() with no parameter.

### How to keep a user's status while he is going to different webpages in the same website?

Use Session Class to store the use's information on the server.

### How to transfer data between webpages?

Webforms are used to transfer information. You can simply append a website's url with "?key=value;key2=value2".

### How to get attributes' name within a relational schema in SQL?

These are stored in SysColumns.
```Select Name FROM SysColumns Where id=Object_Id('TableName');

### How to publich a website?

For ASP.NET websites, use IIS

### access to a database is denied

Use GRANT command to give a user access with SQL queries

### How to write use case?

I searched this topic on the Internet and found lots of good resources. Llike [this](http://www.gatherspace.com/static/use_case_example.html) and [this](https://www.visual-paradigm.com/tutorials/writingeffectiveusecase.jsp)

Also, I have COMP6311 this semester, which is a good source too.

### How to connect a SQL database to .NET applications?

There is a connection string that has to be configured before one can connect to the database. I also learned that .NET doesn't natively support other SQL databases except Sql Server, whose developer is also Microsoft.

As a result, I have to use SQL Server instead of PostgreSQL. Fortunately, they are basically the same.

### How to implement autocomplete function

I searched through the Internet. At first, a tutorial tells me to implement it in JavaScript. However, I didn't know this language and thought it would take me a lot of time to learn this, which the timeframe of the project didn't allow me to do so.

Then, it came to me that this function is actually quite ubquitous because it had been on all kinds of search engines for years. Therefore, I started looking for implemented libraries in order to avoid reinventing wheels.

Luckily, I found the JQuery UI class. After one day reading documentations and tutorials, I finally managed to use the autocomplete module in my project.
