# Meeting 11
- date: 11th September
- participants: Hao, Daniel
---
- worked out the requirements analysis (see the project [landing page](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster))
- Daniel proposed the project's [Purpose](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#purpose), [scope](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#scope), [current system](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#current-system), [proposed system](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#Proposed system), [functional req](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#functional-requirements), [usability](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#usability).
- 2 [senarios](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#scenarios)
- [usecase](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/automatic-reporter---forecaster#use-case-model)