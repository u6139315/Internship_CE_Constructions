create table scratch(
	type varchar(50) check(type in('account', 'project', 'designer')),
	company varchar(50),
	contact varchar(50),
	phone varchar(50),
	email varchar(50),
	address varchar(50)
);