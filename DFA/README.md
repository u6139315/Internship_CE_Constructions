# Testing
1. download [a test file](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/blob/master/DFA/list.csv) and [updateFromCSV.py](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/blob/master/DFA/updateFromCSV.py)
2. open the cmd line
3. type in "py updateFromCSV.py list.csv errorlog.csv" to run the program, where list.csv is the file holds the product data and errorlog.csv is the file records errors.