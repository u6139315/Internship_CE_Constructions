﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class signIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Button_sign_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text == "" || TextBox2.Text == "") return;
        else if (Class1.userIsAuthorized(TextBox1.Text, TextBox2.Text))
        {
            Session["name"] = TextBox1.Text;
            Session["date"] = DateTime.Now.ToShortDateString();
            Response.Redirect("Default.aspx");
        }
        else Label1.Text = "fail please try again";
    }
}