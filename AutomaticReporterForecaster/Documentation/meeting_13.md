Date:9/10/2017

Purpose: check UX, and data fields needed

Expectation: doubts get clarified

Record Keeper: Hao

Attendees: Daniel, Hao

Agenda:
- to improve UX, when a user enters a new call, if the contact or company diesn't exist in the system, the system shall create a new one instead of giving the user an error and letting them go to enter the contact/company themselves.
- more field required with contact: phone, mobile, job title
- also for the UX, drop down lists are not a good idea, because the list could be very long, so a textbox with autofilling is better, just like what a search engine does
- Dan described the workflow as: 
    1. select from [ACCOUNT, PROJECT, DESIGN, COLDCALL, OTHER],
    2. if [PROJECT], tell the user to fill date(default as today), company, project(unique), contact, notes, followUp, quote(maybe/y/n)
    3. else, tell the user to fill what's in step 2 except project(name)