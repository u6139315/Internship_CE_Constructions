# Meeting_9

Venue: 30 Geelong Street, Fyshwick ACT 2609 

Date: 28th August 2017

Participants: Hao, Daniel, Jack

---

The dialogs following are fabricated because I can't recall exactly the same ones.

---

D: Hi, Hao. Have you figured out how to use shortcuts?

H: Oh, I'm sorry. I forgot that. Too many assignments on last week...

D: That's alright. Take your time.

H: I'll look into it. Hopefully, I can have something to show before lunch time.

( ... about 2 hours later)

H: Hey, Daniel. I have made it!

D: Great! Let see what you got.

H: Look at the supplier (folder). Click into it. Then you can see a bunch of shortcuts with 2 kinds of **icons**. This white icon means the URL's is accessible. And the yellow icon means the URL's broken.

D: Fantastic! Icons are better than error logs. You like it, Jack?

J: Yes. They look good.

H: I think it's ready to deliver.

D: Yes, you've almost finished. But before that we still need to check all the suppliers to see the percentage of accessible websites. Do you have some time this afternoon? We can look into the remaining suppliers.

H: Sure thing.

(...after a couple of hours, the investigation is done)

D: Okay. Let's see the percentage. Wow, it's 85%! Considering some suppliers we are not dealing with anymore, the percentage could be even higher!

H: Seems promising.

D: You bet! Can you have a presentation to the management team on Friday? I need you to show your program to the team.

H: No problem.

D: By the way, you may start writing a **user manual** guiding us to use your program. Like adding/deleting products/suppliers, how to interpret the error logs and stuff. Considering we are non-IT people, the manual should free from technical terms and jargons and preferably have some screenshots. #10

H: I got it.

D: You've done an excellent job! Why don't you pack up early today?

H: Awesome!

D: Now, get out of here.