#[Document Fetching Automaton](https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/wikis/document-fetching-automaton)

## Goal Statement

[CE Construction Solutions](http://www.cecsgroup.com.au/about)'s business is to recommend and distribute products from different suppliers to its customers. Hence, the products' information needs to be :

1. up-to-date and
2. easy to retrieve.

Almost every supplier has its own webpages to introduce their products in this day and age. So it's safe to assume the use case is an Internet-based one.

## Stakeholders

The company (acted by CE construction solutions):

- the reputation of the company depends on giving correct information to its customers

Manager of the company (acted by Daniel):

- manually updating product information is tiring and tedious, "is there any way to do it automatically?"

Employees of the manager (acted by David, Robyen, Jack, Michael, etc.):

- a product can be called in different ways. That makes information retrieving difficult.

Sometimes, the manager and employees share the same cases because the organization is a flatten one.

## Scope

| topics                                   | in   | out  |
| ---------------------------------------- | ---- | ---- |
| data-accurateness                        | √    |      |
| reporting errors to inform the administrator | √    |      |
| interface for non-IT people: checking, updating, adding/deleting suppliers/products | √    |      |
| maintaining an archive of folders and files in the OS | √    |      |
| datasheets                               | √    |      |
| Material Safety Data Sheets              | √    |      |
| other files, like CAD blue prints, pictures, broachers |      | √    |
| photocopies of any kind                  |      | √    |

## Functionalities

Potential users consist of the manager and his employees within the company.

### 	Checking and Updating

​		The system should check if all the products kept in the database have the latest description from their suppliers. If not, the system should update both datasheets and MSDS's pertinent to the product. In case of any errors occurred during the process, an error log should be kept to record each error and the product it relates to.

### 	Adding

​		The system adds a record of a product in response to the user's command. The details include name of the supplier, name of the product and URL string of the product's datasheet. If the same product has already existed in the database, the system should raise a warning to the user and ask whether to proceed.

### 	Deleting

​		The system deletes an existing record of a product or products of an existing supplier in response to the user's command. In both cases, the system should firstly check the product or supplier is in the database. The system should be able to through out any error occurs during the process.

###     Altering
.       The system change information stored. It should be able to change the product name, the supplier name and the URL.

### 	Retrieving

​		The system takes in some select condition (e.g. supplier's name is XYZ, product name is UVW, etc.) as input and returns records of all applicable products(i.e. their suppliers, names, URL's).

## Usecases

1. Assume the system has just been delivered to the user. Then the user need to transfer his product information into the system. At present, the information the user already has are suppliers' and products' name. Therefore, URL's are missing and need to be recovered by the user. After getting the missing URL's, the user may now begin to type in all the product information into the system using the Adding functionality.
2. After the system is initialized, it's ready to function. The user may use Retrieving function to get any product information he typed in previously.
3. Time elipses easily, therefore, the user may want to know wheather the information he has is up-to-date. Then, he use the Checking and Updating function to see if there is any information he has to update manually according to the error log generated in the process.
4. Now the user has found some expired information and wants to delete it. Then he uses the Deleting function to do so.
5. For some URL's that have expired, they can be fixed by the user. Just use the Altering function.
