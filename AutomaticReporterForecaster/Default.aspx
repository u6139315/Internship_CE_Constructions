﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html>

<html>
    <head>
        <title>Sales Information Managing Platform</title>
        <link href="StyleSheet.css" rel="stylesheet" type="text/css"/>
        <!--jQuery!-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js" ></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" ></script>
        <script>
            $(function () {
                $("#TextBox_dr_customer").autocomplete({
                    minLength: 1,
                    autoFocus: true,
                    source: "Handler.ashx",
                    response: function (event, ui) {
                        if (ui.content.length == 0) {
                            $("#customer_details").show();
                        } else {
                            $("#customer_details").hide();
                        }
                    }
                });
                $("#TextBox_dr_number").autocomplete({
                    minLength: 1,
                    autoFocus: true,
                    source: "Handler_quote.ashx",
                    response: function (event, ui) {
                        if (ui.content.length == 0) {
                            $("#quote_details").show();
                            document.getElementById("<%=HiddenField_quote.ClientID%>").value = "new";
                        } else {
                            document.getElementById("<%=HiddenField_quote.ClientID%>").value = "";
                            $("#quote_details").hide();
                        }
                    }
                });
                $("#TextBox_dr_projName").autocomplete({
                    minLength: 1,
                    autoFocus: true,
                    source: "Handler_project.ashx",
                    response: function (event, ui) {
                        if (ui.content.length == 0) {
                            $("#project_details").show();
                            document.getElementById("<%=HiddenField_project.ClientID%>").value = "new";
                        } else {
                            document.getElementById("<%=HiddenField_project.ClientID%>").value = "";
                            $("#project_details").hide();
                        }
                    }
                });
                $("#TextBox_dr_contact2").autocomplete({
                    minLength: 1,
                    autoFocus: true,
                    source: function (request, response) {
                        $.getJSON("Handler_contact.ashx", { customer: $("#<%=TextBox_dr_customer.ClientID%>").val(), term: request.term }, response);
                    },
                    response: function (event, ui) {
                        if (ui.content.length == 0) {
                            document.getElementById("<%=HiddenField_contact.ClientID%>").value = "new";
                            $("#contact_details").show();
                        } else {
                            document.getElementById("<%=HiddenField_contact.ClientID%>").value = "";
                            $("#contact_details").hide();
                        }
                    }
                });
                $("#TextBox_dr_winLoss").autocomplete({
                    minLength: 2,
                    autoFocus: true,
                    source: "Handler_winLoss.ashx"
                });
                $("#DropDownList4").change(function () {
                    if ($("#DropDownList4").val() != "Project") {
                        $("#project").hide();
                        $("#project_details").hide();
                    } else {
                        $("#project").show();
                    }
                });
            });
        </script>
        <style type="text/css">
            .auto-style1 {
                width: 5px;
            }
        </style>
    </head>
<body>
    
    <form runat="server">
        <div id="header">
            <asp:Label ID="Label1" runat="server" Text="Welcome " Font-Size="20px"></asp:Label>
            <a class="logout" href="signIn.aspx">Sign out</a>
            <div class="navbar">
                <div class="dropdown">
                    <button class="dropbtn"><i class="fa fa"></i>Tables</button>
                    <div class="dropdown-content">
                        <a href="Default.aspx?view=5">Customers</a>
                        <a href="Default.aspx?view=3">Projects</a>
                        <a href="Default.aspx?view=0">Contacts</a>
                        <a href="Default.aspx?view=sfsc">Sales calls</a>
                        <a href="Default.aspx?view=quotes">Quotes</a>
                        <a href="Default.aspx?view=h">Personal highlights</a>
                        <a href="Default.aspx?view=i">Industrial information</a>
                        <a href="Default.aspx?view=sys">Core systems</a>
                    </div>
                </div>
                <a href="Default.aspx?view=1">Make sales calls</a>
                <a href="Default.aspx?view=2">Weekly reports</a>
                <div class="dropdown">
                    <button class="dropbtn">Search for<i class="fafa"></i></button>
                    <div class="dropdown-content">
                        <a href="Default.aspx?view=sfq">Quotes</a><br />
                        <a href="Default.aspx?view=sfsc">Sales calls</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="content">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View0" runat="server" OnActivate="View0_Activate">Contacts<br />
                    <table title="Contact" >
                        <tr>
                            <td class="auto-style1">Company</td><td>
                                <asp:TextBox ID="TextBox_contacts_company" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Contact</td><td>
                                <asp:TextBox ID="TextBox_contacts_name" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Job title</td><td>
                                <asp:TextBox ID="TextBox_contacts_jobTitle" runat="server"></asp:TextBox>
                                              </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Phone</td><td>
                                <asp:TextBox ID="TextBox_contacts_phone" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Mobile</td><td>
                                <asp:TextBox ID="TextBox_contacts_mobile" runat="server"></asp:TextBox>
                                           </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">E-mail</td><td>
                                <asp:TextBox ID="TextBox_contacts_email" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Suburb</td><td>
                                <asp:TextBox ID="TextBox_contacts_suburb" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Street</td><td>
                                <asp:TextBox ID="TextBox_contacts_street" runat="server"></asp:TextBox>
                                           </td>
                        </tr>
                        
                        <tr>
                            <td class="auto-style1">
                                <asp:Button ID="Button_0_insert" runat="server" Text="Save" OnClick="Button_0_insert_Click"/>
                            </td>
                        </tr>
                    </table><br />
                    <asp:GridView ID="GridView_contacts" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Company,Name" DataSourceID="SqlDataSource_contacts">
                        <Columns>
                            <asp:BoundField DataField="Company" HeaderText="Company" ReadOnly="True" SortExpression="Company"/>
                            <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name" />
                            <asp:BoundField DataField="jobTitle" HeaderText="jobTitle" SortExpression="jobTitle" />
                            <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                            <asp:BoundField DataField="mobile" HeaderText="mobile" SortExpression="mobile" />
                            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                            <asp:BoundField DataField="suburb" HeaderText="suburb" SortExpression="suburb" />
                            <asp:BoundField DataField="street" HeaderText="street" SortExpression="street" />
                            <asp:CommandField CancelText="cancel" DeleteText="delete" EditText="edit" InsertText="insert" NewText="new" SelectText="select" ShowDeleteButton="True" ShowEditButton="True" UpdateText="update"/>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_contacts" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" DeleteCommand="DELETE FROM [Contacts] WHERE [Company] = @Company AND [Name] = @Name" InsertCommand="INSERT INTO [Contacts] ([Company], [Name], [Phone], [Email], [suburb], [street], [mobile], [jobTitle]) VALUES (@Company, @Name, @Phone, @Email, @suburb, @street, @mobile, @jobTitle)" SelectCommand="SELECT * FROM [Contacts]" UpdateCommand="UPDATE [Contacts] SET [Phone] = @Phone, [Email] = @Email, [suburb] = @suburb, [street] = @street, [mobile] = @mobile, [jobTitle] = @jobTitle WHERE [Company] = @Company AND [Name] = @Name">
                        <DeleteParameters>
                            <asp:Parameter Name="Company" Type="String" />
                            <asp:Parameter Name="Name" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Company" Type="String" />
                            <asp:Parameter Name="Name" Type="String" />
                            <asp:Parameter Name="Phone" Type="String" />
                            <asp:Parameter Name="Email" Type="String" />
                            <asp:Parameter Name="suburb" Type="String" />
                            <asp:Parameter Name="street" Type="String" />
                            <asp:Parameter Name="mobile" Type="String" />
                            <asp:Parameter Name="jobTitle" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Phone" Type="String" />
                            <asp:Parameter Name="Email" Type="String" />
                            <asp:Parameter Name="suburb" Type="String" />
                            <asp:Parameter Name="street" Type="String" />
                            <asp:Parameter Name="mobile" Type="String" />
                            <asp:Parameter Name="jobTitle" Type="String" />
                            <asp:Parameter Name="Company" Type="String" />
                            <asp:Parameter Name="Name" Type="String" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </asp:View>
                <asp:View ID="View1" runat="server" OnActivate="View1_Activate">
                    <!-- new layout!-->
                    <table>
                        <tr>
                            <td>Date</td>
                            <td>
                                <asp:TextBox ID="TextBox_dr_date" runat="server" OnTextChanged="TextBox_dr_date_TextChanged" AutoPostBack="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Call type</td>
                            <td>
                                <asp:DropDownList ID="DropDownList4" runat="server">
                                    <asp:ListItem>Account</asp:ListItem>
                                    <asp:ListItem>Project</asp:ListItem>
                                    <asp:ListItem>Designer</asp:ListItem>
                                    <asp:ListItem>Cold Call</asp:ListItem>
                                    <asp:ListItem>Other</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>Customer</td>
                            <td>
                                <asp:TextBox ID="TextBox_dr_customer" runat="server" OnTextChanged="TextBox_dr_customer_TextChanged" AutoPostBack="true"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="customer_details_js" runat="server"></asp:Label>
                                <table id="customer_details" hidden="hidden" runat="server">
                                    <tr>
                                        <td><asp:HiddenField ID="HiddenField_customer" runat="server" />
                                        Contact</td><td>
                                            <asp:TextBox ID="TextBox_dr_contact" runat="server" AutoCompleteType="None" autocomplete="off" disableautocomplete></asp:TextBox></td>
                                        <td>Core systems</td><td>
                                            <asp:TextBox ID="TextBox_dr_coreSys" runat="server"></asp:TextBox></td>
                                        <td>Monthly spend</td><td>
                                            <asp:TextBox ID="TextBox_dr_monthlySpend" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td><td>
                                            <asp:TextBox ID="TextBox_dr_phone" runat="server" AutoCompleteType="None" autocomplete="off" ></asp:TextBox></td>
                                        <td>Suburb</td><td>
                                            <asp:TextBox ID="TextBox_dr_suburb" runat="server"></asp:TextBox></td>
                                        <td>Street</td><td>
                                            <asp:TextBox ID="TextBox_dr_street" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr><td>Website</td><td>
                                        <asp:TextBox ID="TextBox_dr_website" runat="server"></asp:TextBox></td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="project" hidden="hidden">
                            <td><asp:HiddenField ID="HiddenField_project" runat="server"/>Project</td><td>
                                <asp:TextBox ID="TextBox_dr_projName" runat="server" OnTextChanged="TextBox_dr_projName_TextChanged" AutoPostBack="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="project_detais_js" runat="server"></asp:Label>
                                <table id="project_details" hidden="hidden">
                                    <tr>
                                        <td>Suburb</td><td>
                                            <asp:TextBox ID="TextBox_dr_pSuburb" runat="server"></asp:TextBox></td>
                                        <td>Street</td><td>
                                            <asp:TextBox ID="TextBox_dr_pStreet" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Company</td><td>
                                            <asp:TextBox ID="TextBox_dr_pCompany" runat="server"></asp:TextBox></td>
                                        <td>Contact</td><td>
                                            <asp:TextBox ID="TextBox_dr_pContact" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Core systems</td><td>
                                            <asp:TextBox ID="TextBox_dr_pCore" runat="server"></asp:TextBox></td>
                                        <td>Estimated value</td><td>
                                            <asp:TextBox ID="TextBox_dr_pValue" runat="server"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    
                        <tr><td>Contact<asp:HiddenField ID="HiddenField_contact" runat="server" /></td><td>
                            <asp:TextBox ID="TextBox_dr_contact2" runat="server" AutoPostBack="true" OnTextChanged="TextBox_dr_contact2_TextChanged"></asp:TextBox></td></tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="contact_details_js" runat="server"></asp:Label>
                                <table id="contact_details" hidden="hidden">
                                    <tr>
                                        <td>Job title</td><td>
                                                    <asp:TextBox ID="TextBox_dr_jobTitle" runat="server"></asp:TextBox></td>
                                        <td>Phone</td><td>
                                                    <asp:TextBox ID="TextBox_dr_phone2" runat="server"></asp:TextBox></td>
                                                <td>Mobile</td><td>
                                                    <asp:TextBox ID="TextBox_dr_mobile" runat="server"></asp:TextBox></td>
                                                   </tr>
                                     <tr>
                                         <td>Email</td><td>
                                             <asp:TextBox ID="TextBox_dr_email" runat="server"></asp:TextBox></td>
                                         <td>Suburb</td><td>
                                                 <asp:TextBox ID="TextBox_dr_suburb2" runat="server"></asp:TextBox></td>
                                         <td>Street</td><td>
                                                 <asp:TextBox ID="TextBox_dr_street2" runat="server"></asp:TextBox></td>
                                     </tr>
                                </table>
                            </td>
                        </tr>
                   
                        <tr>
                            <td>Quote<asp:HiddenField ID="HiddenField_quote" runat="server" /></td><td>
                                <asp:TextBox ID="TextBox_dr_number" runat="server" AutoPostBack="true" OnTextChanged="TextBox_dr_number_TextChanged"></asp:TextBox></td>
                        </tr>
                        <asp:Label ID="quote_details_js" runat="server"></asp:Label>
                        <tr id="quote_details" hidden="hidden"><td></td>
                            <td>
                                <table>
                                    <tr>
                                        <td>Customer</td><td>
                                            <asp:TextBox ID="TextBox_dr_q_customer" runat="server"></asp:TextBox></td>
                                        <td>Value</td><td>
                                            <asp:TextBox ID="TextBox_dr_value" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Product</td><td>
                                            <asp:TextBox ID="TextBox_dr_product" runat="server"></asp:TextBox></td>
                                        <td>Win/Loss</td><td>
                                            <asp:TextBox ID="TextBox_dr_winLoss" runat="server"></asp:TextBox></td>
                                     </tr>
                                </table>
                            </td>
                        </tr>
                    
                        <tr><td>Delegate</td><td><asp:TextBox ID="TextBox_dr_delegate" runat="server"></asp:TextBox></td></tr>
                    </table>
                    <table>
                        <tr><td>Notes</td><td>Personal highlights</td><td>Industry information</td></tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox_dr_notes" runat="server" Height="100px" TextMode="MultiLine"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="TextBox_dr_highlight" runat="server" Height="100px" TextMode="MultiLine"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="TextBox_dr_industry" runat="server" Height="100px" TextMode="MultiLine"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:Button ID="Button5" runat="server" Text="Save" OnClick="Button5_Click"/>
                    <%--implement delete and alter--%>
                    <asp:GridView ID="GridView_calls" runat="server" AutoGenerateColumns="False" DataSourceID="DataSource_calls" AllowPaging="True" AllowSorting="True" OnRowCommand="GridView_calls_RowCommand">
                        <Columns>
                            <%--name and date is not needed--%>
                            <asp:BoundField DataField="CallType" HeaderText="CallType" SortExpression="CallType" />
                            <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
                            <asp:BoundField DataField="Contact" HeaderText="Contact" SortExpression="Contact" />
                            <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes" >
                            <ItemStyle Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FollowUpDeligate" HeaderText="Deligate" SortExpression="FollowUpDeligate" />
                            <asp:BoundField DataField="project" HeaderText="project" SortExpression="project" />
                            <asp:BoundField DataField="quote" HeaderText="quote" SortExpression="quote" />
                            <asp:ButtonField ButtonType="Button" CommandName="d" Text="delete"/>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="DataSource_calls" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" 
                        SelectCommand="SELECT * FROM [dailyReport] WHERE ([Date] = @Date) AND ([Name] = @Name);">
                        <SelectParameters>
                            <%--parameters from session--%>
                            <asp:SessionParameter DbType="Date" Name="Date" SessionField="date" />
                            <asp:SessionParameter DbType="String" Name="Name" SessionField="name" />
                        </SelectParameters>
                        
                    </asp:SqlDataSource>
                </asp:View>
                <asp:View ID="View2" runat="server" OnActivate="View2_Activate">
                    <%--Weekly Reports--%>
                    week ending<asp:DropDownList ID="DropDownList_wr_weekending" runat="server" OnSelectedIndexChanged="View2_Activate" AutoPostBack="true"></asp:DropDownList><hr />
                    Quotes<br />
                    <asp:GridView ID="GridView_wr_quotes" runat="server"></asp:GridView><hr />
                    Major projects<br />
                    <asp:GridView ID="GridView_wr_projects" runat="server"></asp:GridView><hr />
                    New business<br />
                    <asp:GridView ID="GridView_wr_business" runat="server"></asp:GridView><hr />
                    Personal highlights<br />
                    <asp:GridView ID="GridView_wr_highlights" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_highlights">
                        <Columns>
                            <asp:BoundField DataField="content" HeaderText="content" SortExpression="content" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_highlights" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" SelectCommand="SELECT [content] FROM [Highlights]"></asp:SqlDataSource>
                    <hr />
                    Industry information<br />
                    <asp:GridView ID="GridView_wr_industry" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_indInfo">
                        <Columns>
                            <asp:BoundField DataField="content" HeaderText="content" SortExpression="content" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_indInfo" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" SelectCommand="SELECT [content] FROM [IndustryInfo]"></asp:SqlDataSource>
                    <hr />
                    Key items for follow up<br />
                    <asp:GridView ID="GridView_wr_keyItemForFollowUp" runat="server"></asp:GridView>
                </asp:View>
                <asp:View ID="View3" runat="server">
                    <span>PROJECTS<br /></span>
                    <table>
                        <tr>
                            <td>Name</td>
                            <td><asp:TextBox ID="TextBox_proj_name" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Suburb</td>
                            <td><asp:TextBox ID="TextBox_proj_suburb" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Street</td>
                            <td><asp:TextBox ID="TextBox_proj_street" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Customer</td>
                            <td><asp:TextBox ID="TextBox_proj_customer" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Contact</td>
                            <td><asp:TextBox ID="TextBox_proj_contact" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Estimated value</td>
                            <td><asp:TextBox ID="TextBox_proj_monthlySpend" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                    <!--project!-->
                    <asp:Button ID="Button_proj_save" runat="server" Text="save" OnClick="Button_proj_save_Click"/>
                    <asp:GridView ID="GridView_project" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="name" DataSourceID="SqlDataSource_projects">
                        <Columns>
                            <asp:BoundField DataField="name" HeaderText="name" ReadOnly="True" SortExpression="name" />
                            <asp:BoundField DataField="suburb" HeaderText="suburb" SortExpression="suburb" />
                            <asp:BoundField DataField="street" HeaderText="street" SortExpression="street" />
                            <asp:BoundField DataField="company" HeaderText="company" SortExpression="company" />
                            <asp:BoundField DataField="contact" HeaderText="contact" SortExpression="contact" />
                            <asp:BoundField DataField="estValue" HeaderText="estValue" SortExpression="estValue" />
                            <asp:CommandField CancelText="cancel" DeleteText="delete" EditText="edit" InsertText="insert" NewText="new" SelectText="seect" ShowDeleteButton="True" ShowEditButton="True" UpdateText="update" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_projects" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" SelectCommand="SELECT * FROM [Project]" DeleteCommand="DELETE FROM [Project] WHERE [name] = @name" InsertCommand="INSERT INTO [Project] ([name], [suburb], [street], [company], [contact], [estValue]) VALUES (@name, @suburb, @street, @company, @contact, @estValue)" UpdateCommand="UPDATE [Project] SET [suburb] = @suburb, [street] = @street, [company] = @company, [contact] = @contact, [estValue] = @estValue WHERE [name] = @name">
                        <DeleteParameters>
                            <asp:Parameter Name="name" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter Name="suburb" Type="String" />
                            <asp:Parameter Name="street" Type="String" />
                            <asp:Parameter Name="company" Type="String" />
                            <asp:Parameter Name="contact" Type="String" />
                            <asp:Parameter Name="estValue" Type="Decimal" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="suburb" Type="String" />
                            <asp:Parameter Name="street" Type="String" />
                            <asp:Parameter Name="company" Type="String" />
                            <asp:Parameter Name="contact" Type="String" />
                            <asp:Parameter Name="estValue" Type="Decimal" />
                            <asp:Parameter Name="name" Type="String" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </asp:View>
                <asp:View ID="View_sfsc" runat="server">
                    customer<asp:TextBox ID="TextBox_sfsc_customer" runat="server"></asp:TextBox>
                    project<asp:TextBox ID="TextBox_sfsc_project" runat="server"></asp:TextBox>
                    start<asp:TextBox ID="tb_sfsc_start" runat="server"></asp:TextBox>
                    end<asp:TextBox ID="tb_sfsc_end" runat="server"></asp:TextBox>
                    <asp:Button ID="Button_sfsc_search" runat="server" Text="Search" OnClick="Button_sfsc_search_Click" />
                    <asp:GridView ID="GridView_sfsc" runat="server" OnRowCommand="GridView_sfsc_RowCommand">
                        <Columns>
                            <asp:ButtonField CommandName="d" ButtonType="Button" Text="delete"/>
                        </Columns>
                    </asp:GridView>
                </asp:View>
                <asp:View ID="View5" runat="server">
                    <span style="font-size:20px">COMPANIES</span><br />
                    <table title="COMPANIES">
                        <tr>
                            <td>Customer</td>
                            <td><asp:TextBox ID="TextBox_company_name" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Contact</td>
                            <td><asp:TextBox ID="TextBox_company_contact" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Monthly spend</td>
                            <td><asp:TextBox ID="TextBox_company_monthlySpend" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><asp:TextBox ID="TextBox_company_phone" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Website</td>
                            <td><asp:TextBox ID="TextBox_company_website" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Suburb</td>
                            <td><asp:TextBox ID="TextBox_company_suburb" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Street</td>
                            <td><asp:TextBox ID="TextBox_company_street" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                    <!--introducer can be retrieved from session and regDate can be Now-->
                    <asp:Button ID="Button_company_insert" runat="server" Text="Save" OnClick="Button_company_insert_Click" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="please enter a company name!" ControlToValidate="TextBox_company_name"></asp:RequiredFieldValidator><br />
                    <%--new view--%>
                    <asp:GridView ID="GridView_companies" runat="server" AutoGenerateColumns="False" DataKeyNames="name" DataSourceID="SqlDataSource_companies" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="name" HeaderText="name" ReadOnly="True" SortExpression="name" />
                            <asp:BoundField DataField="contact" HeaderText="contact" SortExpression="contact" />
                            <asp:BoundField DataField="mSpend" HeaderText="mSpend" SortExpression="mSpend" />
                            <asp:BoundField DataField="phone" HeaderText="phone" SortExpression="phone" />
                            <asp:BoundField DataField="website" HeaderText="website" SortExpression="website" />
                            <asp:BoundField DataField="suburb" HeaderText="suburb" SortExpression="suburb" />
                            <asp:BoundField DataField="street" HeaderText="street" SortExpression="street" />
                            <asp:BoundField DataField="introducer" HeaderText="introducer" SortExpression="introducer" />
                            <asp:BoundField DataField="regDate" DataFormatString="{0:yyyy/mm/dd}" HeaderText="regDate" SortExpression="regDate" />
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" CancelText="cancel" DeleteText="delete" EditText="edit" InsertText="insert" NewText="new" SelectText="select" UpdateText="update" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_companies" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" 
                        DeleteCommand="DELETE FROM [companies] WHERE [name] = @name" 
                        InsertCommand="INSERT INTO [companies] ([name], [contact], [mSpend], [phone], [website], [suburb], [street], [introducer], [regDate]) VALUES (@name, @contact, @mSpend, @phone, @website, @suburb, @street, @introducer, @regDate)" 
                        SelectCommand="SELECT * FROM [companies]" 
                        UpdateCommand="UPDATE [companies] SET [contact] = @contact, [mSpend] = @mSpend, [phone] = @phone, [website] = @website, [suburb] = @suburb, [street] = @street, [introducer] = @introducer, [regDate] = @regDate WHERE [name] = @name">
                        <DeleteParameters>
                            <asp:Parameter Name="name" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter Name="contact" Type="String" />
                            <asp:Parameter Name="mSpend" Type="Decimal" />
                            <asp:Parameter Name="phone" Type="String" />
                            <asp:Parameter Name="website" Type="String" />
                            <asp:Parameter Name="suburb" Type="String" />
                            <asp:Parameter Name="street" Type="String" />
                            <asp:Parameter Name="introducer" Type="String" />
                            <asp:Parameter DbType="Date" Name="regDate" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="contact" Type="String" />
                            <asp:Parameter Name="mSpend" Type="Decimal" />
                            <asp:Parameter Name="phone" Type="String" />
                            <asp:Parameter Name="website" Type="String" />
                            <asp:Parameter Name="suburb" Type="String" />
                            <asp:Parameter Name="street" Type="String" />
                            <asp:Parameter Name="introducer" Type="String" />
                            <asp:Parameter DbType="Date" Name="regDate" />
                            <asp:Parameter Name="name" Type="String" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </asp:View>
                <asp:View ID="view_quotes" runat="server">quotes<br />
                    
                    <!--name!-->
                    <!--date!-->
                    <table>
                        <tr>
                            <td>number</td>
                            <td><asp:TextBox ID="TextBox_q_number" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>customer</td>
                            <td><asp:TextBox ID="TextBox_q_customer" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>value</td>
                            <td><asp:TextBox ID="TextBox_q_value" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>product</td>
                            <td><asp:TextBox ID="TextBox_q_product" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>winloss</td>
                            <td><asp:TextBox ID="TextBox_q_winloss" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:Button ID="Button_q_add" runat="server" Text="Add"  OnClick="Button_q_add_Click"/><br />
                    <asp:GridView ID="GridView_quotes"
                        runat="server" 
                        AutoGenerateColumns="False" 
                        DataKeyNames="number" 
                        DataSourceID="SqlDataSource_quotes">
                        <Columns>
                            <asp:BoundField DataField="name" HeaderText="SalesRep" SortExpression="name" />
                            <asp:BoundField DataField="date" HeaderText="date" SortExpression="date" DataFormatString="{0:yyyy/mm/dd}"/>
                            <asp:BoundField DataField="number" HeaderText="number" ReadOnly="True" SortExpression="number" />
                            <asp:BoundField DataField="customer" HeaderText="customer" SortExpression="customer" />
                            <asp:BoundField DataField="product" HeaderText="product" SortExpression="product" />
                            <asp:BoundField DataField="value" HeaderText="value" SortExpression="value" />
                            <asp:BoundField DataField="winLoss" HeaderText="winLoss" SortExpression="winLoss" />
                            <asp:CommandField CancelText="cancel" DeleteText="delete" EditText="edit" InsertText="insrt" NewText="new" SelectText="select" ShowDeleteButton="True" ShowEditButton="True" UpdateText="update" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource
                        ID="SqlDataSource_quotes" 
                        runat="server" 
                        ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" 
                        SelectCommand="SELECT * FROM [quotes]" DeleteCommand="DELETE FROM [quotes] WHERE [number] = @number" InsertCommand="INSERT INTO [quotes] ([number], [name], [date], [customer], [value], [product], [winLoss]) VALUES (@number, @name, @date, @customer, @value, @product, @winLoss)" UpdateCommand="UPDATE [quotes] SET [name] = @name, [date] = @date, [customer] = @customer, [value] = @value, [product] = @product, [winLoss] = @winLoss WHERE [number] = @number">
                        <DeleteParameters>
                            <asp:Parameter Name="number" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="number" Type="String" />
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter Name="date" Type="String" />
                            <asp:Parameter Name="customer" Type="String" />
                            <asp:Parameter Name="value" Type="Decimal" />
                            <asp:Parameter Name="product" Type="String" />
                            <asp:Parameter Name="winLoss" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter Name="date" Type="String" />
                            <asp:Parameter Name="customer" Type="String" />
                            <asp:Parameter Name="value" Type="Decimal" />
                            <asp:Parameter Name="product" Type="String" />
                            <asp:Parameter Name="winLoss" Type="String" />
                            <asp:Parameter Name="number" Type="String" />
                        </UpdateParameters>
                        
                    </asp:SqlDataSource>
                </asp:View>
                <asp:View ID="view_sq" runat="server"> 
                    Customer<asp:TextBox CssClass="customer" ID="TextBox_sq_customer" runat="server"></asp:TextBox>
                    Value at least<asp:TextBox ID="TextBox_sq_value_lower" runat="server">0</asp:TextBox>
                    
                    From<asp:TextBox ID="TextBox_sq_start" runat="server"></asp:TextBox>
                    To<asp:TextBox ID="TextBox_sq_end" runat="server"></asp:TextBox>
                    <asp:Button ID="Button_sq" runat="server" Text="Search" OnClick="Button_sq_Click"/>
                    <asp:GridView ID="GridView_sq" runat="server" OnRowCommand="GridView_sq_RowCommand">
                        <Columns>
                            <asp:ButtonField ButtonType="Button" CommandName="d" Text="delete"/>
                        </Columns>
                    </asp:GridView>
                </asp:View>
                <asp:View ID="view_sys" runat="server">
                    Core systems of projects<br />
                    <table>
                        <tr>
                            <td>Project</td><td><asp:TextBox ID="TextBox_sys_proj" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>System</td><td><asp:TextBox ID="TextBox_sys_system" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:Button ID="Button_sys_proj" runat="server" Text="add" OnClick="Button_sys_proj_Click" />
                    <asp:GridView ID="GridView_projSys" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_projSys" AllowPaging="True" AllowSorting="True" OnRowCommand="GridView_projSys_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="project" HeaderText="Project" ReadOnly="true" SortExpression="project" />
                            <asp:BoundField DataField="coreSystem" HeaderText="Core Systems" ReadOnly="true" SortExpression="coreSystem" />
                            <asp:ButtonField ButtonType="Button" CommandName="del" Text="delete" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_projSys" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" SelectCommand="SELECT * FROM [ProjectSystems]"></asp:SqlDataSource>
                    <hr />
                    <table>
                        <tr>
                            <td>Customer</td><td>
                                <asp:TextBox ID="TextBox_sys_customer" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Core system</td><td>
                                <asp:TextBox ID="TextBox_sys_system2" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:Button ID="Button_sys_customer" runat="server" Text="add" OnClick="Button_sys_customer_Click"/>
                    Core systems of customers<br />
                    <asp:GridView ID="GridView_cpnSys" runat="server" AutoGenerateColumns="False" DataKeyNames="company" DataSourceID="SqlDataSource_cpnSys" OnRowCommand="GridView_cpnSys_RowCommand" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="company" HeaderText="company" ReadOnly="True" SortExpression="company" />
                            <asp:BoundField DataField="coreSystem" HeaderText="coreSystem" SortExpression="coreSystem" />
                            <asp:ButtonField ButtonType="Button" CommandName="del" Text="delete" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_cpnSys" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" SelectCommand="SELECT * FROM [CompanySystems]"></asp:SqlDataSource>
                </asp:View>
                <asp:View ID="View_highlights" runat="server">
                    <span style="font-size:20px">Personal highlights</span>
                    <table>
                        <tr>
                            <td>Name</td>
                            <td>
                                <asp:TextBox ID="TextBox_highlights_name" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Date</td>
                            <td>
                                <asp:TextBox ID="TextBox_highlights_date" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Highlight</td>
                            <td>
                                <asp:TextBox ID="TextBox_highlights_content" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:Button ID="Button_highlight_add" runat="server" Text="Button" OnClick="Button_highlight_add_Click"/>
                    <asp:GridView ID="GridView_highlights" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="name,date" DataSourceID="SqlDataSource_highlights_editable">
                        <Columns>
                            <asp:BoundField DataField="name" HeaderText="name" ReadOnly="True" SortExpression="name" />
                            <asp:BoundField DataField="date" HeaderText="date" ReadOnly="True" SortExpression="date" />
                            <asp:BoundField DataField="content" HeaderText="content" SortExpression="content" />
                            <asp:CommandField CancelText="cancel" DeleteText="delete" EditText="edit" InsertText="insert" NewText="new" SelectText="select" ShowDeleteButton="True" ShowEditButton="True" UpdateText="update" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_highlights_editable" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" DeleteCommand="DELETE FROM [Highlights] WHERE [name] = @name AND [date] = @date" InsertCommand="INSERT INTO [Highlights] ([name], [date], [content]) VALUES (@name, @date, @content)" SelectCommand="SELECT * FROM [Highlights]" UpdateCommand="UPDATE [Highlights] SET [content] = @content WHERE [name] = @name AND [date] = @date">
                        <DeleteParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter DbType="Date" Name="date" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter DbType="Date" Name="date" />
                            <asp:Parameter Name="content" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="content" Type="String" />
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter DbType="Date" Name="date" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </asp:View>
                <asp:View ID="View_industry" runat="server">
                    <table>
                        <tr>
                            <td>Name</td>
                            <td>
                                <asp:TextBox ID="TextBox_i_name" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Date</td>
                            <td>
                                <asp:TextBox ID="TextBox_i_date" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Industry information</td>
                            <td>
                                <asp:TextBox ID="TextBox_i_content" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:Button ID="Button_i" runat="server" Text="add" OnClick="Button_i_Click"/>
                    <asp:GridView ID="GridView_i" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="name,date" DataSourceID="SqlDataSource_i_edit">
                        <Columns>
                            <asp:BoundField DataField="name" HeaderText="name" ReadOnly="True" SortExpression="name" />
                            <asp:BoundField DataField="date" HeaderText="date" ReadOnly="True" SortExpression="date" />
                            <asp:BoundField DataField="content" HeaderText="content" SortExpression="content" />
                            <asp:CommandField CancelText="cancel" DeleteText="delete" EditText="edit" InsertText="insert" NewText="new" SelectText="select" ShowDeleteButton="True" ShowEditButton="True" UpdateText="update" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_i_edit" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" DeleteCommand="DELETE FROM [IndustryInfo] WHERE [name] = @name AND [date] = @date" InsertCommand="INSERT INTO [IndustryInfo] ([name], [date], [content]) VALUES (@name, @date, @content)" SelectCommand="SELECT * FROM [IndustryInfo]" UpdateCommand="UPDATE [IndustryInfo] SET [content] = @content WHERE [name] = @name AND [date] = @date">
                        <DeleteParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter DbType="Date" Name="date" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter DbType="Date" Name="date" />
                            <asp:Parameter Name="content" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="content" Type="String" />
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter DbType="Date" Name="date" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </asp:View>
            </asp:MultiView>
        </div>
        <div id="content2">
            <asp:GridView ID="GridView1" runat="server"></asp:GridView>
            
        </div>
        <div id="footer">
            <p id="footer-content">Copyright©2017 Hao Wang</p>
        </div>
    </form>
</body>
</html>
