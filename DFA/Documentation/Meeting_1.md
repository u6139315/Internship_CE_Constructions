# Meeting #1

Venue: 30 Geelong Street, Fyshwick ACT 2609 

Date: 31th July 2017

Participants: Hao, Daniel

---

In this meeting I got the gist of my first project (a.k.a. Document Fetching Automaton).  The dialogs following are fabricated because I can't recall exactly the same ones.

---

D: Good Morning! How are you today?

H: I'm fine. Thank you. And you?

D: Good. Tell me about yourself.

H: ...

( I spent a several minutes bragging about myself... )

D: So, let me introduce you with the business.

H: I'm all ears.

D: As you can see, our company is relatively small. However, we have many customers and suppliers.

H: What do they do with the business?

D: Well, our company provides construction consultation to our customers, selling products of our suppliers.

H: Oh, I see.

D: You know, as the number of our suppliers growing larger and larger, it becomes harder and harder for us **to provide accurate information** to our customers. And that has been a risk of the reputation of the company.

H: Why it's hard to get the info?

D: That's because we currently store all copies of our suppliers' datasheets (or technical sheet) in our servers. Since we don't know when our suppliers might change the original copies, we need to check them from time to time.

![domain diagram](domainDiagram.jpg)

H: How many suppliers and datasheets do you have in total?

D: About 200 and 6000.

H: Wow, that's a lot! That must be very tedious to do the checking.

D: You bet, which brings me to the question that is there any way to solve the problem in a way of an IT person would do? I mean, you are studying computer science, aren't you?

H: Yes, I can do that. Maybe I can come up with a prototype today.

D: Fantastic! Now get to work.

H: I'm on it.

( a couple of hours elapsed...)

H: Hey, I've got a working prototype.

D: I'm impressed! Let's see what it can do.

H: First, you need to create a csv file called 'list.csv', which can be edited by Excel. It look like this (without headers). I have made an example so that we can see how the program runs.

| supplier name | product name | URL        |
| ------------- | ------------ | ---------- |
| supplier_A    | product_x    | http://... |
| supplier_B    | product_y    | http://... |

D: Okay.. What's next?

H: Then you run the Python program, which can automatically download those datasheet from their URL's. Look, here they are!

D: Fantastic! Once we have the list, your program can solve the problem for us?

H: Exactly!

D: Good job! What's time do you have?

H: It's 4 o'clock.

D: Why don't you pack up today? (Considering the excellent job you have done)

H: Awesome! See you on Friday:)

D: See you:)