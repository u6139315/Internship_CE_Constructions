# Details within a shortcut in Windows

Let's first have a look at what is included inside a shortcut.

Create a random shortcut in Windows and try to modify the icon of it. Then open the cmd.exe. Run

```
type abc.url
```

Then you may see:

```
[InternetShortcut]
URL="https://gitlab.cecs.anu.edu.au/u6139315/Internship_CE_Constructions/blob/master/DFA/Documentation/Meeting_10.md"
IconFile=%SystemRoot%\system32\SHELL32.dll
IconIndex=77
```

Now you can see how shortcuts works.

As for "Icon Index" in line 4, here is a [list](http://help4windows.com/windows_7_shell32_dll.shtml) of what icons look like.