# Meeting 10

- date: 4th September
- participants: Hao, Daniel

---

- typical scenario of a sales process
    get products in the design
    
    tell the tender the price of products
    
    sell products to construction project teams/sub contractor
    
- vision of the project
    To store data from weekly reports in a persistent manner, so that it can be easier to retrieve data and get useful insights of improving the business.
    
- sell different types of products in different phases of construction
